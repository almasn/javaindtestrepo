import java.io.*;

public class T {
	public static void main(String[] args) throws Exception {
		T obj = new T();

		//String domainName = "google.com";
		
		//in mac oxs
		//String command = "ping -c 3 " + domainName;

		//in windows
		String command = "java A";
		
		String output = obj.executeCommand(command);

		System.out.println(output);
	}

	private static String executeCommand(String command) {
		StringBuffer output = new StringBuffer();

		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

			String line = "";			

			while ((line = reader.readLine())!= null) {
				output.append(line + "\n");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return output.toString();
	}
}